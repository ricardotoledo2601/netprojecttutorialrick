﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Producto
    {
        private int id; //1 => 4
        private string sku; //9 => 21
        private string nombre; //20 => 43
        private string descripcion; //50 => 103
        private int cantidad; //3 => 6
        private double precio; //5 => 20
                                        //Total => 197

        public Producto() { }
        public Producto(int id, string sku, string nombre, string descripcion, int cantidad, double precio)
        {
            this.Id = id;
            this.Sku = sku;
            this.Nombre = nombre;
            this.Descripcion = descripcion;
            this.Cantidad = cantidad;
            this.Precio = precio;
        }

        public int Id { get => id; set => id = value; }
        public string Sku { get => sku; set => sku = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public double Precio { get => precio; set => precio = value; }

        public override string ToString()
        {
            return Sku + " " + Nombre ;
        }
    }
}

